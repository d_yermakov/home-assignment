import {Directive, ElementRef, HostBinding, Optional, Renderer2, ViewContainerRef} from '@angular/core';
import {FormContainerComponent} from '../form-container/form-container.component';

@Directive({
  selector: '[appTest]'
})
export class TestDirective {

  constructor(
    private elRef: ElementRef<HTMLInputElement>,
    @Optional() private parentCmp: FormContainerComponent,
    private renderer: Renderer2) {
    console.dir(this.elRef.nativeElement);
    this.renderer.setAttribute(this.elRef.nativeElement, 'placeholder', 'vasya');

  }

}
