import {NgModule} from '@angular/core';
import {PartDirective} from './part.directive';
import {EditModeDirective} from './edit-mode.directive';
import {InputComponent} from './input/input.component';
import {MaterialModule} from './material/material.module';
import { TestDirective } from './test.directive';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    PartDirective,
    EditModeDirective,
    InputComponent,
    TestDirective,
    ConfirmDialogComponent
  ],
  imports: [
    MaterialModule,
    CommonModule
  ],
  exports: [
    PartDirective,
    EditModeDirective,
    InputComponent,
    MaterialModule,
    TestDirective
  ],
  entryComponents: [
    ConfirmDialogComponent
  ]
})
export class SharedModule { }
