import {Directive, HostBinding, HostListener, Input, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appEditMode]'
})
export class EditModeDirective {
  @Input() mode: 'edit' | 'view';
  @HostBinding('class.edit')
  get isEdit() {
    return this.mode === 'edit';
  }
  @HostBinding('class.view')
  get isView() {
    return this.mode === 'view';
  }
  constructor(private viewContainerRef: ViewContainerRef) {}
  @HostListener('click', ['$event']) onClick($event) {
    this.mode = this.mode === 'edit' ? 'view' : 'edit';
  }
}
