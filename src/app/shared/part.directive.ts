import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appPart]'
})
export class PartDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
