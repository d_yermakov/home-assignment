import { Component } from '@angular/core';
import {SettingsModel} from './form-container/models/settings.model';
import {ApiService} from './form-container/services/api.service';
import {StateService} from './form-container/services/state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'home-assignment';

  constructor(private apiService: ApiService, private stateService: StateService) {
  }

}
