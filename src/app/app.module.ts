import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormModule} from './form-container/form.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PartDirective } from './shared/part.directive';
import { EditModeDirective } from './shared/edit-mode.directive';
import { InputComponent } from './shared/input/input.component';
import { TestComponent } from './test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
  ],
  imports: [
    BrowserModule,
    FormModule,
    AppRoutingModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
