import {FormCheckService} from './services/form-check.service';
import {MatDialog} from '@angular/material';
import {StateService, viewState} from './services/state.service';
import {ConfirmDialogComponent} from '../shared/confirm-dialog/confirm-dialog.component';
import {DialogType} from '../config/config';
import {StateModel} from './models/state.model';
import {FormContainerComponent} from './form-container.component';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {FormBuilder, Validators} from '@angular/forms';
import {colorPattern, twitterPattern} from './validators/validators';
import {SettingsModel} from './models/settings.model';

@Injectable({
  providedIn: 'root'
})
export class FormHelper {
  constructor(
    private fb: FormBuilder,
    private checkFormService: FormCheckService,
    public dialog: MatDialog,
    private stateService: StateService) {
  }

  initForm(formData: SettingsModel) {
    return this.fb.group({
      general: this.fb.group({
        siteDisplayName: [
          formData.general.siteDisplayName.value, [
            Validators.required, Validators.pattern(/^[a-zA-Z\s]*$/)
          ]
        ],
        defaultLandingPage: [
          formData.general.defaultLandingPage.value, [Validators.required]
        ],
        advertiseContactEmail: [
          formData.general.advertiseContactEmail.value, [
            Validators.required, Validators.email
          ]
        ],
        primarySiteColor: [
          formData.general.primarySiteColor.value, [
            Validators.required, Validators.pattern(colorPattern)
          ]],
        secondarySiteColor: [
          formData.general.secondarySiteColor.value, [
            Validators.required, Validators.pattern(colorPattern)
          ]]
      }),
      newsfeed: this.fb.group({
        iframeProblematicSources: [formData.newsfeed.iframeProblematicSources.value],
        isArticleAutomaticApprovalEnabled: [formData.newsfeed.isArticleAutomaticApprovalEnabled.value],
        minimumArticlesInMyNewsFeed: [formData.newsfeed.minimumArticlesInMyNewsFeed.value, [
          Validators.min(0), Validators.required
        ]]
      }),
      analytics: this.fb.group({
        excludedAgents: [formData.analytics.excludedAgents.value],
        whiteListDomains: [formData.analytics.whiteListDomains.value]
      }),
      social: this.fb.group({
        newsletterDigestPeriod: [formData.social.newsletterDigestPeriod.value],
        facebookEnabled: [formData.social.facebookEnabled.value],
        organizerTwitterLink: [
          formData.social.organizerTwitterLink.value, [
            Validators.required, Validators.pattern(twitterPattern)
          ]
        ]
      }),
    });
  }

  saveData(parentCmp: FormContainerComponent) {
    const formError = this.checkFormService.getFormErrors(parentCmp.form);
    if (formError.length) {
      this.openErrorDialog(formError, parentCmp);
      this.stateService.errorTabsSubject.next(formError);
    } else {
      this.stateService.stateSubject.next('view');
      for (const key1 of Object.keys(parentCmp.form.value)) {
        for (const key2 of Object.keys(parentCmp.form.value[key1])) {
          this.stateService.formData[key1][key2].value = parentCmp.form.value[key1][key2];
        }
      }
      this.stateService.isDataSave.next(viewState.Save);
    }
  }

  openErrorDialog(errorTabs: string[], parentCmp: FormContainerComponent): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        type: DialogType.FormError,
        title: 'Invalid Field',
        text: [
          'One or more of the fields contain invalid values, Please fix invalid values in order to save changes',
        ],
        buttons: [
          'Cancel',
          'Fix invalid values'
        ],
        errorTabs
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        this.cancelData(parentCmp);
      }
    });
  }

  cancelData(parentCmp: FormContainerComponent) {
    this.changeState('view');
    const formValues = {};
    for (const key1 of Object.keys(this.stateService.formData)) {
      formValues[key1] = {};
      for (const key2 of Object.keys(this.stateService.formData[key1])) {
        formValues[key1][key2] = this.stateService.formData[key1][key2].value;
      }
    }
    parentCmp.form.reset(formValues);
    this.stateService.isDataSave.next(viewState.Cancel);
  }

  changeState(mode: StateModel) {
    this.stateService.stateSubject.next(mode);
  }

  openConfirmDialog(parentCmp: FormContainerComponent): Observable<any> {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        type: DialogType.Confirm,
        title: 'Save Changes',
        text: [
          'You have unsaved changes that will be lost if you leave this page.',
          'Do you want to save your changes?'
        ],
        buttons: [
          'Cancel',
          'Save changes'
        ]
      }
    });
    return dialogRef.afterClosed().pipe(map(data => {
      if (data) {
        this.saveData(parentCmp);
      }
      return !data;
    }));
  }
}
