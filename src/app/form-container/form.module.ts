import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormContainerComponent} from './form-container.component';
import {FormRoutingModule} from './form.routing-module';
import { FormHeaderComponent } from './form-header/form-header.component';
import { GeneralComponent } from './components/general/general.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NewsfeedComponent } from './components/newsfeed/newsfeed.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import {HttpClientModule} from '@angular/common/http';
import { AnalyticsComponent } from './components/analitics/analytics.component';
import { SocialComponent } from './components/social/social.component';
import {MaterialModule} from '../shared/material/material.module';
import {FormPartComponent} from './components/form-part/form-part.component';
import {PartDirective} from '../shared/part.directive';
import {EditModeDirective} from '../shared/edit-mode.directive';
import {AppModule} from '../app.module';
import {SharedModule} from '../shared/shared.module';
import {FormHelper} from './form-helper';



@NgModule({
  declarations: [
    FormContainerComponent,
    FormHeaderComponent,
    GeneralComponent,
    NewsfeedComponent,
    SidebarComponent,
    AnalyticsComponent,
    SocialComponent,
    FormPartComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
  ],
  exports: [
  ],
  entryComponents: [
    // GeneralComponent,
    // SocialComponent,
    // NewsfeedComponent,
    // AnalyticsComponent
  ]
})
export class FormModule { }
