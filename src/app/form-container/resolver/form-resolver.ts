import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {StateService} from '../services/state.service';
import {SettingsModel} from '../models/settings.model';
import {ApiService} from '../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class FormResolver implements Resolve<any> {
  constructor(private stateService: StateService, private apiService: ApiService, private router: Router ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    // @todo fix call resolver 2 times when url is .../form
    if (!route.queryParams.tab) {
      this.router.navigate(['/form'], {queryParams: {tab: 'general'}});
    }
    return this.apiService.getData();
  }
}
