import {Component, OnDestroy, OnInit} from '@angular/core';
import {StateService, viewState} from './services/state.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject, Observable, Subject, Subscription} from 'rxjs';
import {SettingsModel} from './models/settings.model';
import {MatDialog} from '@angular/material';
import {FormCheckService} from './services/form-check.service';
import {FormHelper} from './form-helper';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-form-container',
  templateUrl: './form-container.component.html',
  styleUrls: ['./form-container.component.scss']
})
export class FormContainerComponent implements OnInit, OnDestroy {
  objectKeys = Object.keys;
  form: FormGroup;
  tabName: string;
  formData: SettingsModel;
  general = {};
  test: boolean;
  stateSubscription: Subscription[] = [];
  primarySiteColor$ = new BehaviorSubject('');
  secondarySiteColor$ = new BehaviorSubject('');
  qq: Subscription;
  destroySubject$: Subject<void> = new Subject();
  constructor(private stateService: StateService,
              public formHelper: FormHelper,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private checkFormService: FormCheckService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    if (!Object.entries(this.stateService.formData).length) {
      this.stateService.formData = this.route.snapshot.data.data;
    }
    this.primarySiteColor$.next(this.stateService.formData.general.primarySiteColor.value);
    this.secondarySiteColor$.next(this.stateService.formData.general.secondarySiteColor.value);

    this.route.queryParams.pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      this.tabName = (data.tab) ? data.tab : 'general';
    });

    this.form = this.formHelper.initForm(this.stateService.formData);

    this.stateService.state$.pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      data === 'edit' ? this.form.enable() : this.form.disable();
    });

    this.stateService.isDataSave.pipe(takeUntil(this.destroySubject$)).subscribe((data: viewState) => {
      if (data === 'save') {
      }
    });
  }

  stateChage(e) {
    this.stateService.stateSubject.next(e);
  }

  ngOnDestroy(): void {
   this.destroySubject$.next();
   this.destroySubject$.complete();
  }

  isSameFormData() {
    for (const key1 of Object.keys(this.form.value)) {
      for (const key2 of Object.keys(this.form.value[key1])) {
        if (this.stateService.formData[key1][key2].value !== this.form.value[key1][key2]) {
          return false;
        }
      }
    }
    return true;
  }
}
