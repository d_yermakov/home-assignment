export interface SettingsModel {
  general: GeneralModel;
  newsfeed: NewsFeedModel;
  analytics: AnalyticsModel;
  social: SocialModel;
}
interface FormPart<P = any, T = string> {
  description: string;
  values: P;
  value: T;
}

export interface GeneralModel {
  siteDisplayName: FormPart;
  defaultLandingPage: FormPart<[{key: string, value: number}], number>;
  advertiseContactEmail: FormPart;
  primarySiteColor: FormPart;
  secondarySiteColor: FormPart;
}
export interface NewsFeedModel {
  iframeProblematicSources: FormPart;
  isArticleAutomaticApprovalEnabled: FormPart<[{key: 'Yes' | 'No', value: boolean}], boolean>;
  minimumArticlesInMyNewsFeed: FormPart<any, number>;
}
export interface AnalyticsModel {
  excludedAgents: FormPart;
  whiteListDomains: FormPart;
}
export interface SocialModel {
  newsletterDigestPeriod: FormPart<[{key: string; value: number}], number>;
  facebookEnabled: FormPart<[{key: 'Yes' | 'No', value: boolean}]>;
  organizerTwitterLink: FormPart;
}
