import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, Optional, ViewChild} from '@angular/core';
import {InputParams} from '../inputParams';
import {FormContainerComponent} from '../../form-container.component';
import {map, takeUntil} from 'rxjs/operators';
import {StateService} from '../../services/state.service';
import {ArrHelper} from '../arr.helper';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent extends ArrHelper implements OnInit, AfterViewInit {
  agents = [];
  domains = [];

  @ViewChild('agentRef', {static: false}) agentRef: ElementRef<HTMLInputElement>;
  @ViewChild('domainRef', {static: false}) domainRef: ElementRef<HTMLInputElement>;

  constructor( @Optional() public parentCmp: FormContainerComponent, protected stateService: StateService) {
    super(parentCmp, stateService);
  }

  ngOnInit() {
    this.agents = this.parentCmp.form.value.analytics.excludedAgents.split(',');
    this.domains = this.parentCmp.form.value.analytics.whiteListDomains.split(',');
  }

  ngAfterViewInit(): void {
    this.stateService.isDataSave.pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      if (data === 'cancel') {
        this.agents = this.parentCmp.form.value.analytics.excludedAgents.split(',');
        this.domains = this.parentCmp.form.value.analytics.whiteListDomains.split(',');
      }
      if (data === 'save') {
        this.domainRef.nativeElement.value = '';
        this.agentRef.nativeElement.value = '';
      }
    });
  }

}
