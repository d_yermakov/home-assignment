import {Component, Injectable, Input, OnDestroy, Optional} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {FormContainerComponent} from '../form-container.component';
import {map} from 'rxjs/operators';
import {StateService} from '../services/state.service';
import {AnalyticsModel, GeneralModel, NewsFeedModel, SocialModel} from '../models/settings.model';
import {FormHelper} from '../form-helper';
import {Subject} from 'rxjs';

export class InputParams implements OnDestroy {
  @Input() parent: FormGroup = new FormGroup({});
  @Input() partData: NewsFeedModel | SocialModel | GeneralModel | AnalyticsModel;
  @Input() type: string;
  destroySubject$: Subject<void> = new Subject();

  tabData: NewsFeedModel | SocialModel | GeneralModel | AnalyticsModel;

  constructor(@Optional() public parentCmp: FormContainerComponent, protected stateService: StateService, public formHelper?: FormHelper) {
  }

  getData(el: FormContainerComponent, page: string, field: string) {
    return this.stateService.formData[this.parentCmp.tabName][page][field];
  }

  isEditMode() {
    return this.stateService.state$.pipe(map(data => data !== 'edit'));
  }

  ngOnDestroy(): void {
    console.log('destroy', this);
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
