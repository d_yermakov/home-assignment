import {Optional} from '@angular/core';
import {FormContainerComponent} from '../form-container.component';
import {InputParams} from './inputParams';
import {StateService} from '../services/state.service';

export class ArrHelper extends InputParams {
  constructor(@Optional() public parentCmp: FormContainerComponent, protected stateService: StateService) {
    super(parentCmp, stateService);
  }
  addElement(field: string, dataArr: string[], agentRef: HTMLInputElement) {
    dataArr.push(agentRef.value);
    this.parentCmp.form.get(this.parentCmp.tabName).get(field).setValue(dataArr.join());
    agentRef.value = '';
  }

  deleteElement(dataArr: string[], i: number, field: string) {
    dataArr.splice(i, 1);
    this.parentCmp.form.get(this.parentCmp.tabName).get(field).setValue(dataArr.join());
  }
}

