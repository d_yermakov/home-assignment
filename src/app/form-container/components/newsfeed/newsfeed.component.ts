import {Component, Input, OnInit, Optional} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {InputParams} from '../inputParams';
import {FormContainerComponent} from '../../form-container.component';
import {map} from 'rxjs/operators';
import {StateService} from '../../services/state.service';
import {NewsFeedModel, SettingsModel} from '../../models/settings.model';
import {ArrHelper} from '../arr.helper';

@Component({
  selector: 'app-newsfeed',
  templateUrl: './newsfeed.component.html',
  styleUrls: ['./newsfeed.component.scss']
})
export class NewsfeedComponent extends ArrHelper implements OnInit {
  iframes = [];
  approval = [];
  constructor(@Optional() public parentCmp: FormContainerComponent, protected stateService: StateService) {
    super(parentCmp, stateService);
  }

  ngOnInit() {
    this.iframes = this.partData.iframeProblematicSources.value.split(',');
    this.approval = this.partData.isArticleAutomaticApprovalEnabled.values;
    this.stateService.isDataSave.subscribe(data => {
      if (data === 'cancel') {
        this.iframes = this.parentCmp.form.value.newsfeed.iframeProblematicSources.split(',');
      }
    });
  }

}
