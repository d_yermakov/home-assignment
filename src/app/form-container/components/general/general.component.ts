import {AfterContentChecked, AfterContentInit, AfterViewInit, Component, Input, OnInit, Optional, ViewContainerRef} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {StateService} from '../../services/state.service';
import {InputParams} from '../inputParams';
import {FormContainerComponent} from '../../form-container.component';
import {map} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {GeneralModel} from '../../models/settings.model';
import {FormHelper} from '../../form-helper';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent extends InputParams implements OnInit {
  constructor(@Optional() public parentCmp: FormContainerComponent, public stateService: StateService, public formHelper: FormHelper) {
    super(parentCmp, stateService, formHelper);
  }
  // @todo fix reset form value after leave tab and return to previous
  ngOnInit() {
    this.stateService.isDataSave.subscribe(data => {
      if (data === 'cancel') {
        this.parentCmp.primarySiteColor$.next(this.partData.primarySiteColor.value);
        this.parentCmp.secondarySiteColor$.next(this.partData.secondarySiteColor.value);
      }
    });
  }

  click() {
  }

  changeColor(event: any, field: string) {
    if (event.target.type === 'color') {
      this.parentCmp[`${field}$`].next(event.target.value.slice(1));
      (this.parentCmp.form.controls.general as FormGroup).controls[field].setValue(event.target.value.slice(1));
    } else {
      (this.parentCmp.form.controls.general as FormGroup).controls[field].setValue(event.target.value);
      this.parentCmp[`${field}$`].next(event.target.value);
    }
  }

  getColor(field: string): Observable<string> {
    return of('#' + this.getData(this.parentCmp, field, 'value'));
  }

}
