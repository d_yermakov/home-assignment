import {Component, OnInit, Optional} from '@angular/core';
import {InputParams} from '../inputParams';
import {FormContainerComponent} from '../../form-container.component';
import {StateService} from '../../services/state.service';
import {SocialModel} from '../../models/settings.model';
import {ArrHelper} from '../arr.helper';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent  extends ArrHelper implements OnInit {
  // partData: SocialModel;
  constructor(@Optional() public parentCmp: FormContainerComponent, protected stateService: StateService) {
    super(parentCmp, stateService);
  }

  ngOnInit() {
  }

}
