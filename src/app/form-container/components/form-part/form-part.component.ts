import {
  AfterContentInit,
  Component,
  ComponentFactoryResolver,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {InputParams} from '../inputParams';
import {GeneralComponent} from '../general/general.component';
import {PartDirective} from '../../../shared/part.directive';
import {AnalyticsComponent} from '../analitics/analytics.component';
import {NewsfeedComponent} from '../newsfeed/newsfeed.component';
import {SocialComponent} from '../social/social.component';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-form-part',
  templateUrl: './form-part.component.html',
  styleUrls: ['./form-part.component.scss']
})
// export class FormPartComponent extends InputParams implements OnInit, AfterContentInit {
export class FormPartComponent implements OnInit, AfterContentInit {
  @ViewChild('tmpl', {static: true, read: ViewContainerRef}) tmpl: ViewContainerRef;
  cmpArr = {
    general: GeneralComponent,
    analytics: AnalyticsComponent,
    newsfeed: NewsfeedComponent,
    social: SocialComponent
  };
  tabName = 'general';
  constructor(private resolver: ComponentFactoryResolver, private router: ActivatedRoute) {
  }

  ngOnInit() {
    this.router.queryParams.subscribe(data => {
      this.tabName = data.tab;
    });
  }

  ngAfterContentInit(): void {

  }

}
