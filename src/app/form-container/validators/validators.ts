import {AbstractControl} from '@angular/forms';

export const colorPattern = /^([0-9A-F]{6})$/i;
export const twitterPattern = /^(?:http(?:s)?:\/\/)?(?:www\.)?twitter\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-]*)$/;

export function ValidateUrl(control: AbstractControl) {
    if (!control.value.startsWith('https') || !control.value.includes('.io')) {
      return {validUrl: true};
    }
    return null;
  }

export function ValidateParamPresent(control: AbstractControl) {

}

export function ValidateColor(control: AbstractControl) {
  // const colorPattern = /^([0-9A-F]{6})$/ig;
  if (!colorPattern.test(control.value)) {
    return {validColor: true};
  }
  return null;
}

export function ValidateTwitterUrl(control: AbstractControl) {
  // const twitterPattern = /(?:http(?:s)?:\/\/)?(?:www\.)?twitter\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-]*)/;
  if (!twitterPattern.test(control.value)) {
    return {validColor: true};
  }
  return null;
}
