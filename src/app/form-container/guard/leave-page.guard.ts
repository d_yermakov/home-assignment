import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {FormContainerComponent} from '../form-container.component';
import {map, tap} from 'rxjs/operators';
import {StateService} from '../services/state.service';
import {FormHeaderComponent} from '../form-header/form-header.component';

@Injectable({
  providedIn: 'root',
})
export class LeavePageGuard implements CanDeactivate<FormContainerComponent> {
  constructor(private router: Router, private stateService: StateService) {
  }

  canDeactivate(component: FormContainerComponent,
                currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot,
                nextState?: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!component.isSameFormData()) {
      return component.formHelper.openConfirmDialog(component);
    }
    return true;
  }

}
