import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SettingsModel} from '../models/settings.model';

@Injectable({
  providedIn: 'root'
})
export class LocalApiService {
  url = 'assets/data.json';
  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get<SettingsModel>(this.url);
  }
}
