import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {StateModel} from '../models/state.model';
import {ApiService} from './api.service';
import {distinctUntilChanged} from 'rxjs/operators';
import {SettingsModel} from '../models/settings.model';

export enum dataState {
  None = 'none',
  Save = 'save',
  Cancel = 'cancel'
}
export enum viewState {
  Save = 'save',
  Cancel = 'cancel',
  None = 'none'
}
@Injectable({
  providedIn: 'root'
})
export class StateService {
  stateSubject = new BehaviorSubject<StateModel>('view');
  state$ = this.stateSubject.asObservable();
  dataChangeSubject = new BehaviorSubject<boolean>(false);
  isDataChange = this.dataChangeSubject.asObservable();

  errorTabsSubject = new BehaviorSubject<string[]>([]);
  errorTabs$ = this.errorTabsSubject.asObservable().pipe(distinctUntilChanged());

  isDataSave = new BehaviorSubject<viewState>(viewState.None);

  formData: SettingsModel = {} as SettingsModel;
  constructor(private apiService: ApiService) {
  }
}
