import { Injectable } from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormCheckService {

  constructor() { }

  getFormErrors(form: FormGroup) {
    const errorTabs = [];
    if (!form.valid) {
      for (const key of Object.keys(form.controls)) {
        if (form.controls[key].invalid) {
          errorTabs.push(key);
        }
      }
    }
    return errorTabs;
  }
}
