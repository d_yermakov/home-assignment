import { Injectable } from '@angular/core';
import {LocalApiService} from './local-api.service';
import {Observable} from 'rxjs';
import {SettingsModel} from '../models/settings.model';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(private localApi: LocalApiService) { }

  getData(): Observable<SettingsModel> {
    return this.localApi.getData();
  }

  get settings() {
    return this.getData();
  }
}
