import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormContainerComponent} from './form-container.component';
import {LeavePageGuard} from './guard/leave-page.guard';
import {FormResolver} from './resolver/form-resolver';

const routes: Routes = [
  {
    path: 'form',
    component: FormContainerComponent,
    resolve: {
      data: FormResolver
    },
    canDeactivate: [
      LeavePageGuard
    ]
  },
  // {
  //   path: '**',
  //   redirectTo: 'form'
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormRoutingModule { }
