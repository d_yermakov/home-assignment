import {Component, Input, OnInit} from '@angular/core';
import {StateService} from '../services/state.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() tab = 'general';
  tabs = ['general', 'newsfeed', 'social', 'analytics'];
  constructor(public stateService: StateService) { }

  ngOnInit() {
    this.stateService.errorTabsSubject.next([]);
  }

}
