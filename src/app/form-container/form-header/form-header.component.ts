import {Component, EventEmitter, OnInit, Optional, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {StateModel} from '../models/state.model';
import {StateService} from '../services/state.service';
import {FormContainerComponent} from '../form-container.component';
import {FormCheckService} from '../services/form-check.service';
import {MatDialog} from '@angular/material';
import {FormHelper} from '../form-helper';

@Component({
  selector: 'app-form-header',
  templateUrl: './form-header.component.html',
  styleUrls: ['./form-header.component.scss']
})
export class FormHeaderComponent implements OnInit {
  state: Observable<StateModel>;
  @Output() emitChangeState = new EventEmitter<string>();
  constructor(
    public formHelper: FormHelper,
    public stateService: StateService,
    @Optional() public parentCmp: FormContainerComponent,
    public checkFormService: FormCheckService) { }

  ngOnInit() {
    this.state = this.stateService.state$;
    this.parentCmp.form.valueChanges.subscribe(data => {
      if (this.parentCmp.isSameFormData()) {
        this.stateService.dataChangeSubject.next(true);
      } else {
        this.stateService.dataChangeSubject.next(false);
      }
    });
    for (const tabCtrl in this.parentCmp.form.controls) {
      if (this.parentCmp.form.controls.hasOwnProperty(tabCtrl)) {
        this.parentCmp.form.controls[tabCtrl].statusChanges.subscribe(data => {
            const formError = this.checkFormService.getFormErrors(this.parentCmp.form);
            this.stateService.errorTabsSubject.next(formError);
        });
      }
    }
  }

}
