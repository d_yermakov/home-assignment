export enum DialogType {
  Confirm = 'confirm-closed',
  FormError = 'form-error'
}
